'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrganizationsSchema extends Schema {
  up () {
    this.create('organizations', (table) => {
      table.increments()
      table.string('uuid').unique().index()
      table.string('name')
      table.string('email')
      table.string('website')
      table.timestamps()
    })
  }

  down () {
    this.drop('organizations')
  }
}

module.exports = OrganizationsSchema
