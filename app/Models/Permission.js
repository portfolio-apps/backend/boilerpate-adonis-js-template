'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Permission extends Model {

  organization() {
    return this.belongsTo('App/Models/Organization')
  }

  role() {
    return this.belongsTo('App/Models/Role')
  }

}

module.exports = Permission
