'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Role extends Model {

  organization() {
    return this.belongsTo('App/Models/Organization')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  permissions() {
    return this.hasMany('App/Models/Permission')
  }

}

module.exports = Role
