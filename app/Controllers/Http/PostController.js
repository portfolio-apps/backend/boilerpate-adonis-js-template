'use strict'

// Models
const Post = use('App/Models/Post')

// Validator
const { validate } = use('Validator')

class PostController {

  /**------------------------------------------
   *  List All Posts
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
  async index({ view, request, response }) {
    // Get ALL Posts
    const posts = await Post.query().with('user').fetch()

    // Get the request URL
    const url = request.url()

    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(posts)
    } else {
      return view.render('posts.index', {
        title: "Latest Posts",
        posts: posts.toJSON()
      })
    }
  }
 
  /**------------------------------------------
   *  Show Post Details
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
  async details({ params, view }) {
    // Find post
    const post = await Post.find(params.id)

    if (!post) {
      return response.status(404).json({
        success: false,
        message: `Post ${params.id} does not exist`
      })
    }

    // return post with view
    return view.render('posts.details', {
      post: post
    })
  }

  /**------------------------------------------
   *  Add Post View
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
   async add({ view }) {
    return view.render('posts.add')
  }

  /**------------------------------------------
   *  Store Post
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
   async store({ request, response, session }) {
    // Validate
    const validation = await validate(request.all(), {
      title: 'required|min:3|max:255',
      body: 'required|min:3'
    })
    // Check if validation failed
    if(validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }
    // Create new instance
    const post = new Post();
    // Assign detials
    post.title = request.input('title')
    post.body = request.input('body')
    // Save instance
    await post.save()
    // Show user success
    session.flash({ notification: "Post added!" })
    // return to page
    return response.redirect('/posts')
  }

  async create({request, auth, response}) {
    
    try {
      // if (await auth.check()) {
      let post = await auth.user.posts().create(request.all())
      await post.load('user');
      return response.json(post)
      // }

    } catch (e) {
      console.log(e)
      return response.json({message: 'You are not authorized to perform this action'})
    }

  }

  /**------------------------------------------
   *  Edit Post View
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
   async edit({ params, view }) {
    const post = await Post.find(params.id)
    return view.render('posts.edit', {
      post: post
    })
  }

  /**------------------------------------------
   *  Update Post
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
   async update({ params, request, response, session }) {
    // Validate
    const validation = await validate(request.all(), {
      title: 'min:3|max:255',
      body: 'min:3'
    })
    // Check if validation failed
    if(validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }
    // Create new instance
    const post = await Post.find(params.id)

    if (!post) {
      return response.status(404).json({
        success: false,
        message: `Post ${params.id} does not exist`
      })
    }

    // Assign detials
    post.title = request.input('title')
    post.body = request.input('body')

    // Save instance
    await post.save()
    // Show user success
    session.flash({ notification: "Post updated!" })

    // Get the request URL
    const url = request.url()

    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(post)
    } else {
      return response.redirect('/posts')
    }
  }

  /**------------------------------------------
   *  Delete Post
   * ------------------------------------------
   * @param {*} param0 
   * @returns 
   */
   async destroy({ params, session, request, response }) {
     // Find instance
    const post = await Post.find(params.id)

    if (!post) {
      return response.status(404).json({
        success: false,
        message: `Post ${params.id} does not exist`
      })
    }

    // Delete instance
    await post.delete()
    // Show user success
    session.flash({ notification: "Post deleted!" })

    // Get the request URL
    const url = request.url()

    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json({
        success: true,
        message: "Post deleted"
      })
    } else {
      // return to page
      return response.redirect('/posts')
    }    
  }
}

module.exports = PostController
