'use strict'

const Redis = use('Redis')
const User = use('App/Models/User')

class UserController {

  async redis ({ response }) {
    
    const test = {
      success: true,
      message: "Redis is running"
    }

    await Redis.set("foo", JSON.stringify(test))

    const val = await Redis.get('foo')
    return response.json(val);
  }
}

module.exports = UserController
